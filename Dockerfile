FROM ubuntu:14.04
RUN apt-get update
RUN apt-get -y install curl nano
RUN curl -LO http://dev.mysql.com/get/mysql-apt-config_0.3.2-1ubuntu14.04_all.deb
RUN dpkg -i mysql-apt-config_0.3.2-1ubuntu14.04_all.deb

RUN apt-get update && apt-get install mysql-workbench -y

RUN apt-get install unixodbc unixodbc-dev freetds-dev freetds-bin tdsodbc -y

ADD odbcinst.ini /etc/odbcinst.ini
ADD odbc.ini /etc/odbc.ini 
ADD freetds.conf /etc/freetds/freetds.conf

ENTRYPOINT ["mysql-workbench"]


